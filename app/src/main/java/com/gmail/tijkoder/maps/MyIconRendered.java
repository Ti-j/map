package com.gmail.tijkoder.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gmail.tijkoder.maps.data.db.entity.MarkerEntity;
import com.gmail.tijkoder.maps.utils.LogUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class MyIconRendered extends DefaultClusterRenderer<MarkerEntity> {

    private Context context;
    public MyIconRendered(Context context, GoogleMap map,
                          ClusterManager<MarkerEntity> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        setMinClusterSize(1);//min размер кластера для исключения наложений фото
    }


    @Override
    protected void onBeforeClusterItemRendered(MarkerEntity item,
                                               MarkerOptions markerOptions) {
        Bitmap icon = ThumbnailUtils.extractThumbnail(
                BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.tux), 100, 100);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected void onClusterItemRendered(MarkerEntity item, final Marker marker) {
        super.onClusterItemRendered(item, marker);
        Glide.with(context)
                .load(item.getUri())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.tux)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(new SimpleTarget<Drawable>(100, 100) {    //from glide API
                    @Override
                    public void onResourceReady(@NonNull Drawable resource,
                                                @Nullable Transition<? super Drawable> transition) {
                        marker.setIcon(getMarkerIconFromDrawable(resource, null));
                    }
                });
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MarkerEntity> cluster,
                                           MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);

        Bitmap icon = ThumbnailUtils.extractThumbnail(
                BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.tux), 100, 100);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected void onClusterRendered(Cluster<MarkerEntity> cluster, final Marker marker) {
        super.onClusterRendered(cluster, marker);

        final Integer size = cluster.getItems().size();
        LogUtils.printLog( "size of cluster: " + size.toString());
        String uri = "";
        for(MarkerEntity mMarker : cluster.getItems()){
            uri = mMarker.getUri();
            if(uri.length() > 0) break;
        }

        Glide.with(context)
                .load(uri)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.tux)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(new SimpleTarget<Drawable>(100, 100) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource,
                                                @Nullable Transition<? super Drawable> transition) {
                        marker.setIcon(getMarkerIconFromDrawable(resource, size.toString()));
                    }
                });
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable, String text) {
        Paint fontPaint;
        int fontSize = 40;
        fontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fontPaint.setTextSize(fontSize);
        fontPaint.setColor(Color.RED);
        fontPaint.setStyle(Paint.Style.STROKE);

        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        if (text != null) canvas.drawText(text, 5, 100, fontPaint);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

}
