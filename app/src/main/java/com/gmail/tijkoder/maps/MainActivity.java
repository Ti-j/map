package com.gmail.tijkoder.maps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.gmail.tijkoder.maps.fragments.MySupportMapFragment;
import com.gmail.tijkoder.maps.utils.LogUtils;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 269;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!cheakPermission(this)) {    // Разрешение не предоставлено
            LogUtils.printLog("!cheakPermission");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this,
                        "Для работы приложения требуется разрешение доступа к памяти",
                        Toast.LENGTH_LONG).show();
                LogUtils.printLog("Need Permission");
            } else {
                LogUtils.printLog("requestMultiplePermissions");
                requestMultiplePermissions();   //получить разрешения приложения
            }
        } else {    // Разрешение предоставлено
            LogUtils.printLog("cheakPermission");
            if (savedInstanceState == null) {
                mapInit();
            }
        }
    }

    private void mapInit() {
        MySupportMapFragment mapFragment = new MySupportMapFragment();
        mapFragment.setRetainInstance(true);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frgmCont, mapFragment);
        fragmentTransaction.commit();
        mapFragment.GetMyMapAsync();
    }

    // результат запроса на разрешения
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        mapInit();
                    } else {
                        Toast.makeText(this,
                                "Для работы приложения требуется разрешение доступа к памяти",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
        }
    }

    //запрос на разрешения для приложения
    public void requestMultiplePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    private boolean cheakPermission(Context context) {
        return (ContextCompat.checkSelfPermission
                (context, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }

}