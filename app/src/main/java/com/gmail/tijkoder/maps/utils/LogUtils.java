package com.gmail.tijkoder.maps.utils;


import android.util.Log;

import com.gmail.tijkoder.maps.BuildConfig;

public class LogUtils {
    private static final String LOG_TAG = "DebugMap";
    public static void printLog(String value) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, value);
        }
    }
}
