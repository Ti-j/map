package com.gmail.tijkoder.maps.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.gmail.tijkoder.maps.data.db.dao.MarkerDao;
import com.gmail.tijkoder.maps.data.db.entity.MarkerEntity;

@Database(entities = {MarkerEntity.class}, version = 1)
public abstract class MarkerDatabase extends RoomDatabase {
    public abstract MarkerDao markerDao();
}
