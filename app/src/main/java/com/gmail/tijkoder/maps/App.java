package com.gmail.tijkoder.maps;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.gmail.tijkoder.maps.data.db.MarkerDatabase;

public class App extends Application {
    public static App instance;
    private MarkerDatabase markerDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        markerDatabase = Room.databaseBuilder(this, MarkerDatabase.class,
                "markerDatabase")
                .fallbackToDestructiveMigration()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public MarkerDatabase getMarkerDatabase() {
        return markerDatabase;
    }
}
