package com.gmail.tijkoder.maps.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.tijkoder.maps.MyListAdapter;
import com.gmail.tijkoder.maps.R;
import com.gmail.tijkoder.maps.utils.LogUtils;

import java.util.ArrayList;

public class ListFragment extends Fragment implements MyListAdapter.CallBackMyList{

    View view;
    ArrayList<String> uriList;
    public static ListFragment newInstance(ArrayList<String> uriList){
        ListFragment listFragment = new ListFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("uriList", uriList);
        listFragment.setArguments(args);
        return listFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        uriList = getArguments().getStringArrayList("uriList");
        RecyclerView recyclerView = view.findViewById(R.id.rv_items);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity().
                getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        MyListAdapter myListAdapter = new MyListAdapter(uriList, ListFragment.this);
        recyclerView.setAdapter(myListAdapter);
    }

    @Override
    public void callBackOnClick(String strUri) {
        LogUtils.printLog( "Clicked item uri: " + strUri);
        ImgFragment fragImg = ImgFragment.newInstance(strUri);
        FragmentTransaction fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, fragImg);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }
}
