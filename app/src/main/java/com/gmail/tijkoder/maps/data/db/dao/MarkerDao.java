package com.gmail.tijkoder.maps.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.gmail.tijkoder.maps.data.db.entity.MarkerEntity;

import java.util.List;

@Dao
public interface MarkerDao {

    @Query("SELECT * FROM markerEntity WHERE keyGeo = 1")
    List<MarkerEntity> getItemsWithGeo();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MarkerEntity markerEntity);

    @Update
    void update(MarkerEntity markerEntity);

    @Delete
    void delete(MarkerEntity markerEntity);

    @Query("DELETE FROM markerEntity WHERE sessionTime NOT LIKE :sessionTime")
    void deleteLostItems(long sessionTime);
}
