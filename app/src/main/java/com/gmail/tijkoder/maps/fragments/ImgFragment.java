package com.gmail.tijkoder.maps.fragments;

import android.support.v4.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gmail.tijkoder.maps.R;


public class ImgFragment extends Fragment {

    public static ImgFragment newInstance(String uri){
        ImgFragment imgFragment = new ImgFragment();
        Bundle args = new Bundle();
        args.putString("URL",uri);
        imgFragment.setArguments(args);
        return imgFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.img_fragment, container, false);
        ImageView imageView = view.findViewById(R.id.img);

        if(!TextUtils.isEmpty(getArguments().getString("URL"))){
            Glide.with(getActivity().getApplicationContext())
                    .load(Uri.parse("file://" + getArguments().getString("URL")))
                    .apply(new RequestOptions()
                            // изображение, отображаемое до загрузки необходимого.
                            .placeholder(R.drawable.tux)
                            .fitCenter()
                            .dontAnimate())
                     .into(imageView);
        }
        return view;
    }
}
