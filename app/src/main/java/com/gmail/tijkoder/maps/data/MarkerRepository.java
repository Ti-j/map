package com.gmail.tijkoder.maps.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.gmail.tijkoder.maps.App;
import com.gmail.tijkoder.maps.data.db.MarkerDatabase;
import com.gmail.tijkoder.maps.data.db.entity.MarkerEntity;

import java.util.List;

public class MarkerRepository {

    private MarkerDatabase database = App.getInstance().getMarkerDatabase();
    private MutableLiveData<List<MarkerEntity>> listLiveData;

    public LiveData<List<MarkerEntity>> getItemsWithGeo() {
        if (listLiveData == null) {
            listLiveData = new MutableLiveData<>();
            TaskGetItemsWithGeo taskGetItemsWithGeo = new TaskGetItemsWithGeo();
            taskGetItemsWithGeo.execute();
        }
        return listLiveData;
    }

    public void add(MarkerEntity entity){
        TaskAdd taskAdd = new TaskAdd();
        taskAdd.execute(entity);
    }

    public void deleteLostItems(long sessionTime) {
        TaskDeleteLostItems taskDeleteLostItems = new TaskDeleteLostItems();
        taskDeleteLostItems.execute(sessionTime);
    }

    private class TaskGetItemsWithGeo extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            listLiveData.postValue(database.markerDao().getItemsWithGeo());
            return null;
        }
    }


    private class TaskAdd extends AsyncTask<MarkerEntity, Void, Void> {

        @Override
        protected Void doInBackground(MarkerEntity... markerEntities) {
            for (MarkerEntity markerEntity : markerEntities) {
                database.markerDao().insert(markerEntity);
            }
            return null;
        }
    }

    private class TaskDeleteLostItems extends AsyncTask<Long, Void, Void> {

        @Override
        protected Void doInBackground(Long... longs) {
            database.markerDao().deleteLostItems(longs[0]);
            listLiveData.postValue(database.markerDao().getItemsWithGeo());
            return null;
        }
    }
}
