package com.gmail.tijkoder.maps.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.gmail.tijkoder.maps.App;
import com.gmail.tijkoder.maps.GeoDegree;
import com.gmail.tijkoder.maps.MyIconRendered;
import com.gmail.tijkoder.maps.R;
import com.gmail.tijkoder.maps.data.ImageRepository;
import com.gmail.tijkoder.maps.data.MarkerRepository;
import com.gmail.tijkoder.maps.data.db.entity.MarkerEntity;
import com.gmail.tijkoder.maps.utils.LogUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MySupportMapFragment extends SupportMapFragment {

    private ClusterManager<MarkerEntity> clusterManager;
    private MarkerRepository markerRepository;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
//        setRetainInstance(true);
        markerRepository = new MarkerRepository();
        if (bundle == null) {
            Thread mThreadScanMedia = new Thread(
                    new Runnable() {
                        public void run() {
                            ScanMedia();
                        }
                    }
                    );
            mThreadScanMedia.start();
        }
    }

    public void ReplaceFragment(Fragment fragment) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frgmCont, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
    }

    @Override
    public void getMapAsync(OnMapReadyCallback onMapReadyCallback) {
        super.getMapAsync(onMapReadyCallback);
    }

    public void GetMyMapAsync() {
        super.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LogUtils.printLog("Map Ready");
                clusterManager = new ClusterManager<>(
                        App.getInstance(),
                        googleMap);
                googleMap.setOnCameraIdleListener(clusterManager);  //CallBack после движения камеры
                clusterManager.setRenderer(new MyIconRendered(
                        App.getInstance(),
                        googleMap, clusterManager));
                clusterManager.setOnClusterItemClickListener(
                        new ClusterManager.OnClusterItemClickListener<MarkerEntity>() {
                            @Override
                            public boolean onClusterItemClick(MarkerEntity MarkerEntity) {
                                String strUri = MarkerEntity.getUri();
                                LogUtils.printLog("Clicked item uri: "
                                        + MarkerEntity.getUri());
                                ReplaceFragment(ImgFragment.newInstance(strUri));
                                return false;
                            }
                        });
                clusterManager.setOnClusterClickListener(
                        new ClusterManager.OnClusterClickListener<MarkerEntity>() {
                            @Override
                            public boolean onClusterClick(Cluster<MarkerEntity> cluster) {
                                Integer size = cluster.getSize();
                                LogUtils.printLog("Size of clicked cluster: " + size.toString());

                                ArrayList<String> uriList = new ArrayList<>();
                                Collection<MarkerEntity> markers = cluster.getItems();
                                for (MarkerEntity myMarker : markers) {
                                    uriList.add(myMarker.getUri());
                                }
                                ReplaceFragment(ListFragment.newInstance(uriList));
                                return false;
                            }
                        });
                addMarkers(googleMap);
            }
        });
    }

    private void addMarkers(GoogleMap googleMap){
        LiveData<List<MarkerEntity>> entityLiveData;
        googleMap.setOnMarkerClickListener(clusterManager);
        entityLiveData = markerRepository.getItemsWithGeo();
        entityLiveData.observe(this, new Observer<List<MarkerEntity>>() {
            @Override
            public void onChanged(@Nullable List<MarkerEntity> markerEntities) {
                clusterManager.clearItems();
                clusterManager.addItems(markerEntities);
                clusterManager.cluster();
            }
        });
    }


    private void ScanMedia() {
        LogUtils.printLog("Start ScanMedia");
        long sessionTime = new Date().getTime();
        ImageRepository imageRepository = new ImageRepository(getContext());
        List<String> absolutePathOfImages = imageRepository.getAbsolutePathOfImages();
        if(absolutePathOfImages.size() > 0) {
            for (String absolutePathOfImage : absolutePathOfImages) {
                int IndexDot = absolutePathOfImage.lastIndexOf('.');
                if (IndexDot > 0) {
                    // расширение файла из полного пути в нижнем регистре
                    String extension = absolutePathOfImage.substring(IndexDot + 1).toLowerCase();
                    //проверяем расширение, т.к. гео теги работают для jpeg
                    if (extension.endsWith("jpg") || extension.endsWith("jpeg")) {
                        try { //получим exif данные - gps
                            GeoDegree GeoDegree;
                            ExifInterface exif;
                            MarkerEntity markerEntity;
                            File photoFile = new File(absolutePathOfImage);
                            exif = new ExifInterface(photoFile.getCanonicalPath());
                            GeoDegree = new GeoDegree(exif);  //гео тег
                            markerEntity = new MarkerEntity(
                                    absolutePathOfImage,
                                    GeoDegree.getLatitude(),
                                    GeoDegree.getLongitude(),
                                    GeoDegree.isValid(),
                                    sessionTime);
                            markerRepository.add(markerEntity);
                        } catch (IOException e) {
                            Log.e("IOException", "file not found");
                            e.printStackTrace();
                        }
                    }
                }
            }
            markerRepository.deleteLostItems(sessionTime);
        }
    }
}
