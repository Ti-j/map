package com.gmail.tijkoder.maps.data.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

@Entity(indices = {@Index(value = "uri", unique = true)})
public class MarkerEntity implements ClusterItem {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "uri")
    private String uri;

    private double  latitude, longitude;
    private boolean keyGeo;
    private long sessionTime;

    public MarkerEntity(String uri, double latitude, double longitude, boolean keyGeo,
                       long sessionTime) {
        this.uri = uri;
        this.latitude = latitude;
        this.longitude = longitude;
        this.keyGeo = keyGeo;
        this.sessionTime = sessionTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isKeyGeo() {
        return keyGeo;
    }

    public void setKeyGeo(boolean keyGeo) {
        this.keyGeo = keyGeo;
    }

    public long getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(int sessionTime) {
        this.sessionTime = sessionTime;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude,longitude);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
