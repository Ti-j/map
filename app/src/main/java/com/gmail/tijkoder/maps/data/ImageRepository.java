package com.gmail.tijkoder.maps.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.gmail.tijkoder.maps.BuildConfig;
import com.gmail.tijkoder.maps.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

public class ImageRepository {

    private List<String> absolutePathOfImages = new ArrayList<>();
    final private Uri uri;

    public ImageRepository(Context context) {
        uri =  MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        debugMessages(context);
        Cursor cursor = context.getContentResolver().query(
                uri, new String[]{MediaStore.MediaColumns.DATA},
                null, null, null);
        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfImages.add(cursor.getString(column_index_data));
        }
        cursor.close();
    }

    public List<String> getAbsolutePathOfImages() {
        return absolutePathOfImages;
    }

    private void debugMessages(Context context) {
        if (BuildConfig.DEBUG) {
            Cursor DebugCursor = context.getContentResolver().query(
                    uri, new String[]{"MAX(" + BaseColumns._ID + ")"},
                    null, null, null);
            if (DebugCursor != null && DebugCursor.getCount() > 0) {
                DebugCursor.moveToFirst();
                LogUtils.printLog("Max id of ContentResolver: " + DebugCursor.getString(0));
                DebugCursor.close();
            } else {
                if (DebugCursor == null) {
                    LogUtils.printLog("DebugCursor - null");
                }
                if (DebugCursor.getCount() == 0) {
                    LogUtils.printLog("DebugCursor.getCount() == 0");
                }
            }
        }
    }
}
