package com.gmail.tijkoder.maps;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gmail.tijkoder.maps.utils.LogUtils;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> items;
    private Context context;
    private Fragment fragment;

    public interface CallBackMyList {
        void callBackOnClick(String strUri);
    }

    public MyListAdapter(ArrayList<String> items, Fragment fragment){
        this.items = items;
        this.fragment = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item, parent,false);
        return new MyListAdapter.MyListHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Glide.with(context)
                .load(this.items.get(position))
                .apply(new RequestOptions()
                        // изображение, отображаемое до загрузки необходимого.
                        .placeholder(R.drawable.tux)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform()
                        )
                .into(((MyListAdapter.MyListHolder)holder).mImageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView mImageView;

        MyListHolder(View v){
            super(v);
            mImageView = v.findViewById((R.id.item_photo));
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            LogUtils.printLog("onClick");
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION){
                String str = items.get(position);
                LogUtils.printLog("clicked: " + str);
                CallBackMyList callBackMyList;
                try {
                    callBackMyList = (CallBackMyList) fragment;
                } catch (ClassCastException e) {
                    throw new ClassCastException(context.toString() + " must implement callBackMyList");
                }
                callBackMyList.callBackOnClick(str);
            }
        }
    }
}
